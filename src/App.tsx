import React from 'react';
import './App.css';

import attn_white_logo from './assets/ATTN_White_Logo.png'
import evos_logo from './assets/EVOS_Logo.png'
import whim_logo from './assets/WHIM_Logo.png'
import attn_tech_logo from './assets/ATTN_TECH_logo.png'
import ilustration_01 from './assets/Illustrasi_01.png'
import ilustration_02 from './assets/Illustrasi_02.png'
import linkedin from './assets/Linkedin_Icon.png'
import facebook from './assets/Facebook_Icon.png'
import twitter from './assets/Twitter_Icon.png'
import youtube from './assets/Youtube_Icon.png'
import pin from './assets/Pin_Location_Jobs_bar.png'
import asean_map from './assets/ASEAN_Maps.png'

const divisions = [
  {
    logo: evos_logo,
    name: 'Esports',
    desc: 'The world of Esports athletes, tournaments, and titles.',
    link: 'https://www.evos.gg/'
  },
  {
    logo: whim_logo,
    name: 'Entertainment',
    desc: 'Influencing consumers and growing community through multiple pathways.',
    link: 'https://www.whim.sg/'
  },
  {
    logo: attn_tech_logo,
    name: 'Tech',
    desc: 'Strive to lead in the innovation invention, and development.',
    link: 'https://attnte.ch/'
  }
]

const ilustrations = [
  [
    {
      title: 'A place where passion lives and dreams are built.',
      subtitle: 'ATTN was built on the foundational belief that the world will be a better place if anyone and everyone were given an opportunity to pursue a career in the interests that they love.'
    },
    {
      image: ilustration_01
    }
  ],
  [
    {
      image: ilustration_02
    },
    {
      title: 'Join our movement to keep ATTN roaring!',
      subtitle: 'With your hard work, passion, and determination, you will help us to shape the future of eSports. Are you up to the challenge?'
    }
  ]
]

const social = [
  {
    image: linkedin,
    link: 'https://www.linkedin.com/company/evosesports/'
  },
  {
    image: facebook,
    link: 'https://www.facebook.com/teamEVOS'
  },
  {
    image: twitter,
    link: 'https://twitter.com/evosesports'
  },
  {
    image: youtube,
    link: 'https://www.youtube.com/channel/UC7B2kbI32oz3RSB5CsbPgkQ'
  }
]

interface IPosting {
  additionalPlai: string
  additional: string
  categories: {
    commitment: string
    department: string
    location: string
    team: string
  }
  createdAt: number
  descriptionPlain: string
  description: string
  id: string
  lists: Array<{
    text: string
    content: string
  }>
  text: string
  hostedUrl: string
  applyUrl: string
}

function App() {
  const [postings, setPostings] = React.useState<Array<IPosting> | undefined>()
  const [locations, setLocations] = React.useState<Array<string> | undefined>()
  const [departments, setDepartments] = React.useState<Array<string> | undefined>()
  const [teams, setTeams] = React.useState<Array<string> | undefined>()
  const [workTypes, setWorkTypes] = React.useState<Array<string> | undefined>()
  const [location, setLocation] = React.useState<string | undefined>()
  const [department, setDepartment] = React.useState<string | undefined>()
  const [team, setTeam] = React.useState<string | undefined>()
  const [workType, setWorkType] = React.useState<string | undefined>()
  const [filteredPostings, setFilteredPostings] = React.useState<Array<IPosting> | undefined>()
  

  React.useEffect(() => {
    fetch('https://api.lever.co/v0/postings/attn?mode=json')
    .then((res) => res.json())
    .then((data: Array<IPosting>) => {
      setPostings(data)
      const l : Array<string> = []
      const d : Array<string> = []
      const t : Array<string> = []
      const wt : Array<string> = []
      data.forEach((data) => {
        if (!l.includes(data.categories.location)) l.push(data.categories.location)
        if (!d.includes(data.categories.department)) d.push(data.categories.department)
        if (!t.includes(data.categories.team)) t.push(data.categories.team)
        if (!wt.includes(data.categories.commitment)) wt.push(data.categories.commitment)
      })
      setLocations(l)
      setDepartments(d)
      setTeams(t)
      setWorkTypes(wt)
    })
  }, [])

  React.useEffect(() => {
    let filtered = postings
    if (location) {
      filtered = filtered?.filter((p) => p.categories.location.includes(location))
    }
    if (department) {
      filtered = filtered?.filter((p) => p.categories.department.includes(department))
    }
    if (team) {
      filtered = filtered?.filter((p) => p.categories.team.includes(team))
    }
    if (workType) {
      filtered = filtered?.filter((p) => p.categories.commitment.includes(workType))
    }
    setFilteredPostings(filtered)
  }, [location, department, workType, team, postings])

  return (
    <>
      <section className='section-explore'>
        <div className='container'>
          <img src={attn_white_logo} alt="ATTN White Logo" />
          <h1>EXPLORE <strong>ATTN</strong></h1>
          <div className='division-container'>
            {
              divisions.map(d => {
                return <div className='division-item' key={d.name}>
                  <img src={d.logo} alt={`${d.name} logo`} />
                  
                  <span className='desc'>{d.desc}</span>
                  <a href={d.link} className='link'>Learn More</a>
                </div>
              })
            }
          </div>
          {
            ilustrations.map((il, idx) => {
              return (
                <div className="ilustration-container" key={`il_${idx}`}>
                  {
                    il.map((item, iidx) => {
                      return (
                        <div className="ilustration-item" key={`il_item_${iidx}`}>
                          {
                            item.image ? (
                              <img src={item.image} alt={`ilustrasi`} />
                            ) : undefined
                          }
                          {
                            item.title ? <span className='title'>{item.title}</span> : undefined
                          }
                          {
                            item.subtitle ? <span className='subtitle'>{item.subtitle}</span> : undefined
                          }
                        </div>
                      )
                    })
                  }
                </div>
              )
            })
          }
        </div>
      </section>
      <section className="section-posting">
        <div className="container">
          <div className="posting-filters">
            <div className="posting-filters-container">
              <div className="row">
                <div className="col">
                  <span>See our job openings!</span>
                </div>
                <div className="col">
                  <button 
                    type="button" 
                    onClick={() => {
                      setDepartment('')
                      setLocation('')
                      setTeam('')
                      setWorkType('')
                    }}
                  >
                    Clear All Filters
                  </button>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <select value={location} placeholder="Filter by Location" onChange={(e) => setLocation(e.target.value)}>
                    {
                      [undefined, ...(locations || [])].map((l) => <option key={`location_${l}`} value={l || ''}>{l || 'Filter by Location'}</option>)
                    }
                  </select>
                </div>
                <div className="col">
                  <select value={department} placeholder="Filter by Department" onChange={(e) => setDepartment(e.target.value)}>
                    {
                      [undefined, ...(departments || [])].map((l) => <option key={`department_${l}`} value={l || ''}>{l || 'Filter by Department'}</option>)
                    }
                  </select>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <select value={team} placeholder="Filter by Team" onChange={(e) => setTeam(e.target.value)}>
                    {
                      [undefined, ...(teams || [])].map((l) => <option key={`team_${l}`} value={l || ''}>{l || 'Filter by Team'}</option>)
                    }
                  </select>
                </div>
                <div className="col">
                  <select value={workType} placeholder="Filter by Work Type" onChange={(e) => setWorkType(e.target.value)}>
                    {
                      [undefined, ...(workTypes || [])].map((l) => <option key={`worktype_${l}`} value={l || ''}>{l || 'Filter by Work Type'}</option>)
                    }
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="posting-list">
            {
              filteredPostings?.map((p) => {
                return (
                  <div className="posting-item" key={p.createdAt}>
                    <div className="posting-position">
                      <span className="posting-team">{p.categories.team}</span>
                      <span className="posting-text">{p.text}</span>
                    </div>
                    <div className="posting-location">
                      <img src={pin} alt={p.categories.location} />
                      <span>{p.categories.location}</span>
                    </div>
                    <div className="posting-apply">
                      <a href={p.hostedUrl}>
                        Apply
                      </a>
                    </div>
                  </div>
                )
              })
            }
            {
              filteredPostings?.length === 0 ? <div className="posting-item">No result</div> : undefined
            }
          </div>
          <div className="office-container">
            <div className="office-meta">
              <span className="title">Our Offices</span>
              <span className="subtitle">
                Our team is growing across Southeast Asia, and we now operate in 5 different countries, including 
                <strong> Indonesia, Singapore, Malaysia, Thailand, and Vietnam.</strong>
              </span>
            </div>
            <img src={asean_map} alt="asean map" />
          </div>
        </div>
      </section>
      <section className='section-footer'>
        <div className='container'>
          <div className='row'>
            <div className='col'>
              <span className='big-footer'>ATTN.</span>
              <span className='big-footer'>Stay Connected</span>
            </div>
            <div className='col'>
              <div className='row'>
                {
                  social.map(s => {
                    return (
                      <a href={s.link} key={s.link}>
                        <img src={s.image} alt={s.link} />
                      </a>
                    )
                  })
                }
              </div>
            </div>
          </div>
          <div className='row'>
            <span className='copyright'>© 2021 - ATTN, All Right Reserved</span>
          </div>
        </div>
      </section>
    </>
  );
}

export default App;
